## Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# NOTICE: This work was derived from tensorflow/examples/image_retraining
# and modified to use TensorFlow Hub modules.

from glob import glob
import numpy as np
# import tensorflow as tf
import tensorflow.compat.v1 as tf

def create_graph(height, width, depth):
    """ Creates a default graph for image processing. """
    with tf.Graph().as_default() as graph:
        input_tensor = tf.placeholder(tf.float32, [None, height, width, depth])
    return graph, input_tensor

def add_jpeg_decoding(height, width, depth):
    """ Adds operations that perform JPEG decoding and resizing to the graph.
    Args:
        output image size and depth
    Returns:
        input tensor for the node to feed JPEG data into,
        output tensor of the preprocessing steps.
    """
    jpeg_data = tf.placeholder(tf.string, name='JPEGInput')
    decoded_image = tf.image.decode_jpeg(jpeg_data, channels=depth)
    # Convert from full range of uint8 to range [0,1] of float32.
    decoded_image_as_float = tf.image.convert_image_dtype(decoded_image,
                                                          tf.float32)
    decoded_image_4d = tf.expand_dims(decoded_image_as_float, 0)
    resize_shape = tf.stack([height, width])
    resize_shape_as_int = tf.cast(resize_shape, dtype=tf.int32)
    resized_image = tf.image.resize_bilinear(decoded_image_4d,
                                             resize_shape_as_int)
    return jpeg_data, resized_image

def readfile(image_path):
    return tf.gfile.GFile(image_path, 'rb').read()

# MAIN
image_data = [readfile(f) for f in glob('data/*/*')]

image_shape = (512, 512, 3)
graph, resized_image_tensor = create_graph(*image_shape)

with tf.Session(graph=graph) as sess:
    # Initialize session.
    init = tf.global_variables_initializer()
    sess.run(init)

    # Set up the frontline image decoding sub-graph.
    jpeg_data_tensor, decoded_image_tensor = add_jpeg_decoding(*image_shape)
    resized = []
    for image in image_data:
        # First decode the JPEG image, resize it, and rescale the pixel values.
        resized.append(sess.run(decoded_image_tensor,
                                {jpeg_data_tensor: image}))

